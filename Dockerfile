FROM alpine:3.16 as builder

RUN apk add --no-cache python3

WORKDIR /app
RUN python3 -m venv .venv
ENV PATH="/app/.venv/bin:$PATH"

COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -Ur requirements.txt
RUN pip uninstall -y pip

from alpine:3.16

RUN apk add --no-cache python3

WORKDIR /app

COPY --from=builder /app/.venv /app/.venv
ENV PATH="/app/.venv/bin:$PATH"

COPY app.py ./
COPY templates ./templates

RUN addgroup --gid 1001 --system app && \
    adduser --no-create-home --shell /bin/false --disabled-password --uid 1001 --system app app

RUN chown -R app: /app
RUN chmod 400 app.py
USER app

EXPOSE 5002
CMD ["gunicorn", "-w", "1", "-b", "0.0.0.0:5002", "app:app"]

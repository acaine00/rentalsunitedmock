import re
from io import StringIO
from xml.etree.ElementTree import ParseError

from flask import Flask, request, render_template, abort
from defusedxml.ElementTree import parse

app = Flask(__name__)
INFO = 'INFO'


def log(msg, level=INFO):
    print(f'{level}: {msg}', flush=True)


@app.route('/ping/', methods=['GET'])
def ping():
    return 'pong'


@app.route('/', methods=['POST'])
def handle():
    log(f'Handling {request.data}')
    if not request.data:
        abort(400)
    try:
        tree = parse(StringIO(request.data.decode('utf-8')))
        operation = re.match(r'Pull_(\w+)_RQ', tree.getroot().tag).group(1)
        return render_template(f'{operation}.xml'), 200, {'Content-type': 'application/xml; charset=utf-8'}
    except ParseError as e:
        log(f'Unable to parse XML body: {e}')
        abort(400)
